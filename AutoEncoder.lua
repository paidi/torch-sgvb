local AutoEncoder,parent = torch.class('sgvb.AutoEncoder', 'nn.Module')

function AutoEncoder:__init(encoder,decoder)
   parent.__init(self)
   self.encoder = encoder
   self.decoder = decoder  
   self.modules = nn.Sequential():add(self.encoder):add(self.decoder)
end

function AutoEncoder:parameters()
   return self.modules:parameters()
end

function AutoEncoder:reset(stdv)
   self.encoder:reset(stdv)
   self.decoder:reset(stdv)
end

function AutoEncoder:forward(input)
   self.encoding = self.encoder:updateOutput(input)
   self.reconstruction = self.decoder:updateOutput(self.encoding)
   return self.reconstruction, self.encoding
end

function AutoEncoder:backward(input, reconstructionGradient, supervisedGradient)
   self.encodingGradient = self.decoder:updateGradInput(self.encoding, reconstructionGradient)
   if supervisedGradient then self.encodingGradient:add(supervisedGradient) end
   return self.encoder:backward(input, self.encodingGradient)
end
