-- A simple class that allows us to process batches from a table of 
-- sparse tensors
-- 
-- Note (Paidi): should look into extending nn.SparseLinear to handle
-- sparse matrices (i.e. 3D input) and think about porting to cunn
local DocumentEncoder,parent = torch.class('sgvb.DocumentEncoder', 'nn.Sequential')

function DocumentEncoder:__init(vocabSize, outputSize, batchSize)
  parent.__init(self)
  
  -- <batchSize> instances of nn.SparseLinear joined in parallel, all
  -- with the same parameters
  local m = nn.ParallelTable()
  local baseEncoder = nn.SparseLinear(vocabSize,outputSize)
  m:add(nn.Sequential():add(baseEncoder):add(nn.Reshape(1,outputSize)))
  for i=2,batchSize do
    local copyEncoder = nn.SparseLinear(vocabSize,outputSize)
    copyEncoder:share(baseEncoder, 'weight', 'bias', 'gradWeight', 'gradBias')
    m:add(nn.Sequential():add(copyEncoder):add(nn.Reshape(1,outputSize)))
  end
  
  -- Join output into a single matrix in which each row is the
  -- encoding vector for a specific document in the batch
  self:add(m)
  self:add(nn.JoinTable(1))
end
