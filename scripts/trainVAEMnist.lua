require 'nn'
require 'optim'
require 'xlua'
require 'image'

require 'sgvb'

function saveConfig(saveDir, config)
   c_str = ''
   for k,v in pairs(config) do
      c_str = c_str .. ('%s=%s\n'):format(k,v)
   end
   f = io.open(paths.concat(saveDir,'config.txt'), 'w')
   f:write(c_str)
end

function loadMnist(valueType)
   local tar = 'http://torch7.s3-website-us-east-1.amazonaws.com/data/mnist.t7.tgz'

   if not paths.dirp('data/mnist.t7') then
      os.execute('wget ' .. tar)
      os.execute('tar xvf ' .. paths.basename(tar) .. ' -C data')
      os.execute('rm ' .. tar)
   end
   
   local train_file = 'data/mnist.t7/train_32x32.t7'
   local test_file = 'data/mnist.t7/test_32x32.t7'

   local train = torch.load(train_file, 'ascii')
   local test = torch.load(test_file, 'ascii')
   
   --Rescale to 0..1 and invert
   train.data = train.data:double():div(255):resize(60000,1024)
   test.data = test.data:double():div(255):resize(10000,1024)
   
   -- Binarize
   train.data = train.data:double():gt(torch.rand(60000,1024)):double()
   test.data = test.data:gt(torch.rand(10000,1024)):double()

   return {
      train = train.data,
      test = test
   }
end

function createVariationalAE(inputSize, hiddenSize, zSize)
   local function makeLinear(inDim,outDim)
      return nn.Linear(inDim,outDim):reset(0.01 / math.sqrt(3))
   end
   print(('Create %dx%dx%d VAE'):format(inputSize,hiddenSize,zSize))
   local inputLayer = nn.Sequential()
   inputLayer:add(makeLinear(inputSize, hiddenSize))
   inputLayer:add(nn.ReLU())

   -- Hidden to Z's parameters
   local zParam = nn.ConcatTable()
   zParam:add(makeLinear(hiddenSize,zSize))
   zParam:add(makeLinear(hiddenSize,zSize))

   local encoder = nn.Sequential()
   encoder:add(inputLayer)
   encoder:add(zParam)
   
   -- decoder includes random sampling
   local decoder = nn.Sequential()
   decoder:add(sgvb.Gaussian())
   decoder:add(makeLinear(zSize, hiddenSize))
   decoder:add(nn.ReLU())
   decoder:add(makeLinear(hiddenSize,inputSize))
   
   -- Pass through sigmoid to get probability each pixel is set/unset
   decoder:add(nn.Sigmoid())
   
   return sgvb.AutoEncoder(encoder, decoder)
end

cmd = torch.CmdLine()
cmd:option('-train', 'data/mnist/train_32x32.t7', 'data file')
cmd:option('-test', 'data/mnist/test_32x32.t7', 'data file')
cmd:option('-saveDir', 'results', 'directory in which to save model')
cmd:option('-learningRate', 1e-3, 'learning rate at t=0')
cmd:option('-epochs', 1, 'number of epochs')
cmd:option('-batchSize', 1, 'number of tokens per batch')
cmd:option('-hiddenSize', 512, 'size of hidden layer')
cmd:option('-zSize', 128, 'size of stochastic layer')
cmd:option('-progressInterval', 1, 'How often to print progress')
cmd:option('-numClasses', 4, 'How many classes to visualise')
cmd:option('-tieWeights', false, 'Whether the encoder/decoder weights should be tied')
cmd:option('-logGradients', false, 'Should we log gradients during training?')
cmd:option('-valueType', 'double', 'Value type for network parameters')
cmd:option('-displaySamples', false, 'Display samples from model?')
cmd:option('-seed', 12111981, 'Random seen')
cmd:option('-numLatentSamples', 1, 'Number of samples used to generate gradient')
opt = cmd:parse(arg or {})

local saveDir = paths.concat(opt.saveDir, 
                             'mnist-' .. os.date('%y-%m-%d_%H:%M'))

local logger = optim.Logger(paths.concat(saveDir, 'train.log'))
saveConfig(saveDir, opt)
local gradientLogger
if opt.logGradients then
   gradientLogger = optim.Logger(paths.concat(saveDir, 'gradients.log'))
end

torch.manualSeed(opt.seed)

local data = loadMnist(opt.valueType)

local config = { 
   epochs = opt.epochs,
   saveDir = saveDir,
   logger = logger,
   gradientLogger = gradientLogger,
   optimParam = {name = 'adam', learningRate = opt.learningRate},
   numLatentSamples = opt.numLatentSamples
}

if opt.valueType == 'cuda' then
   require 'cutorch'
   require 'cunn'
   torch.setdefaulttensortype('torch.CudaTensor')
   data.train = data.train:cuda()
   data.test.data = data.test.data:cuda()
end

local samplers = {
   sgvb.SimpleSampler(
      data.train,
      data.train,
      opt.batchSize, 
      opt.progressInterval
   ),
   sgvb.SimpleSampler(
      data.test.data:sub(1, data.test.data:size(1) * 0.2),
      data.test.data:sub(1, data.test.data:size(1) * 0.2),
      opt.batchSize, 
      opt.progressInterval
   ),
   sgvb.SimpleSampler(
      data.test.data,
      data.test.data,
      opt.batchSize, 
      opt.progressInterval
   )
}

local lossFns = {nn.BCECriterion(), 
                 sgvb.GaussianKLCriterion()}
lossFns[1].sizeAverage = false

local model = createVariationalAE(1024, opt.hiddenSize, opt.zSize)

sgvb.trainer.train(model, lossFns, samplers, sgvb.ae, config)

-- if opt.zSize == 2 then
--   nn.Visualiser(model.encoder, opt.numClasses):visualise(data.test, data.testLabels)
-- end

torch.setdefaulttensortype('torch.DoubleTensor')
model:double()
local nSamples = 400
local nRow = 20
local zParam = {torch.zeros(nSamples, opt.zSize), torch.ones(nSamples, opt.zSize)}
local samples = model.decoder:forward(zParam)
samples = samples:mul(255):round() --samples:gt(torch.rand(360,32*32))
samples:resize(nSamples, 1, 32, 32)
image.save(paths.concat(saveDir, 'samples.png'), image.toDisplayTensor(samples, 0, nRow))

if opt.displaySamples then
   image.display(samples)
end
