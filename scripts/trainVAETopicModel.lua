require 'nn'
require 'optim'
require 'xlua'
require 'image'
require 'sgvb'

local tablex = require 'pl.tablex'

function saveConfig(saveDir, config)
   c_str = ''
   for k,v in pairs(config) do
      c_str = c_str .. ('%s=%s\n'):format(k,v)
   end
   f = io.open(paths.concat(saveDir,'config.txt'), 'w')
   f:write(c_str)
end

function unigramLoss(train,test,vocabSize)
   local probs = torch.zeros(1,vocabSize)
   
   for _,x in pairs(train) do
      for i=1,x:size(1) do
         probs[{ {1} , {x[{i,1}]} }]:add(x[{i,2}])
      end
   end
   probs:div(probs:sum()):log()

   local cri = sgvb.DocumentCountNLL(true)
   local loss = 0.0
   for _,x in pairs(test) do
      local lx = cri:forward(probs, {x})
      loss = loss + lx
   end
   loss = loss / #test
   return loss, math.exp(loss)
end


function extractTopLabels(multiclassLabels, numClasses)
   -- Find most common labels
   local _,ranked = torch.sort(multiclassLabels:sum(1), true)
   ranked:resize(ranked:size(2))
   local N = multiclassLabels:size(1)
   local m = multiclassLabels:size(2)
   local labelSet  = torch.Tensor(N)
   for i=1,N do
      for j = 1,ranked:size(1) do
         if multiclassLabels[{i,ranked[j]}] == 1 then          
            labelSet[i] = ranked[j]
            break
         end
      end
   end
   
   return labelSet
end

function createEncoder(inputSize, hiddenSizes, zSize, batchSize)
   local numLayers = #hiddenSizes
   local encoder = nn.Sequential()
   encoder:add(sgvb.DocumentEncoder(inputSize, hiddenSizes[1], batchSize))
   encoder:add(nn.ReLU())

   for i=2,numLayers do
      encoder:add(nn.Linear(hiddenSizes[i-1],hiddenSizes[i]))
      encoder:add(nn.ReLU())
   end
   -- Hidden to Z's parameters
   local zParam = nn.ConcatTable()
   zParam:add(nn.Linear(hiddenSizes[numLayers],zSize))
   zParam:add(nn.Linear(hiddenSizes[numLayers],zSize))

   encoder:add(zParam)
   return encoder
end

function createDecoder(inputSize, hiddenSizes, zSize)
   local numLayers = #hiddenSizes
   local decoder = nn.Sequential()

   decoder:add(sgvb.Gaussian())
   decoder:add(nn.Linear(zSize, hiddenSizes[numLayers]))
   decoder:add(nn.ReLU())

   for i=numLayers,2,-1 do
      decoder:add(nn.Linear(hiddenSizes[i],hiddenSizes[i-1]))
      decoder:add(nn.ReLU())
   end

   decoder:add(nn.Linear(hiddenSizes[1],inputSize))
   decoder:add(nn.LogSoftMax())

   return decoder
end

function createVariationalAE(inputSize, hiddenSizes, zSize, batchSize)
   print(('Create %dx%sx%d VAE'):format(inputSize,
                                        table.concat(hiddenSizes, 'x'),
                                        zSize))
   local encoder = createEncoder(inputSize,hiddenSizes,zSize,batchSize)
   -- decoder includes random sampling
   local decoder = createDecoder(inputSize,hiddenSizes,zSize)
   
   return sgvb.AutoEncoder(encoder, decoder)
end

function createEmbedder(encoder, inputSize, hiddenSize, zSize)
   local embedder = createEncoder(inputSize, hiddenSize, zSize, 1)
   local epar = embedder:getParameters()
   local mpar = encoder:getParameters()
   epar:copy(mpar)
   return embedder
end

cmd = torch.CmdLine()
cmd:option('-load', '', 'start training with a given model')
cmd:option('-train', 'data/newsgroups/train.t7', 'data file')
cmd:option('-test', 'data/newsgroups/test.t7', 'data file')
cmd:option('-saveDir', 'results', 'directory in which to save model')
cmd:option('-learningRate', 1e-3, 'learning rate at t=0')
cmd:option('-epochs', 1, 'number of epochs')
cmd:option('-batchSize', 1, 'number of tokens per batch')
cmd:option('-hiddenSizes', '1000x500', 'size of hidden layers')
cmd:option('-zSize', 128, 'size of stochastic layer')
cmd:option('-progressInterval', 1, 'How often to print progress')
cmd:option('-logGradients', false, 'Should we log gradients during training?')
cmd:option('-valueType', 'double', 'Value type for network parameters')
cmd:option('-seed', 12111981, 'Random seen')
cmd:option('-improvementRate', 1, 'Minimum acceptable rate of improvement (used for stopping criteria)')
cmd:option('-numLatentSamples', 1, 'How many samples should be used to approximate the gradient?')
cmd:option('-activation', 'ReLU', 'Activation function for hidden layers')
opt = cmd:parse(arg or {})

local saveDir = paths.concat(opt.saveDir, 
                             'mnist-' .. os.date('%y-%m-%d_%H:%M'))
local logger = optim.Logger(paths.concat(saveDir, 'train.log'))
saveConfig(saveDir, opt)
local gradientLogger
if opt.logGradients then
   gradientLogger = optim.Logger(paths.concat(saveDir, 'gradients.log'))
end

torch.manualSeed(opt.seed)

local data = {}
data.train = torch.load(opt.train)
data.test = torch.load(opt.test)

local config = { 
   epochs = opt.epochs,
   saveDir = saveDir,
   logger = logger,
   gradientLogger = gradientLogger,
   optimParam = {name = 'adagrad', 
                 learningRate = opt.learningRate},
   improvementRate = opt.improvementRate,
   numLatentSamples = opt.numLatentSamples
}

if opt.valueType == 'cuda' then
   require 'cutorch'
   require 'cunn'
   torch.setdefaulttensortype('torch.CudaTensor')
   data.train = data.train:cuda()
   data.test = data.test:cuda()
end

local samplers = {
   sgvb.DocumentCountSampler(
      data.train.counts,
      data.train.vocabSize,
      opt.batchSize, 
      false,
      opt.progressInterval
   ),
   sgvb.DocumentCountSampler(
      tablex.sub(data.test.counts, 1, opt.batchSize, 10*opt.batchSize),
      data.test.vocabSize,
      opt.batchSize, 
      false,
      opt.progressInterval
   )
}

local lossFns = {sgvb.DocumentCountNLL(true), 
                 sgvb.GaussianKLCriterion()}

local model 
if #opt.load > 0 then
   model = torch.load(opt.load)
else
   model= createVariationalAE(data.train.vocabSize, 
                              tablex.map(tonumber, string.split(opt.hiddenSizes, 'x')),
                              opt.zSize, 
                              opt.batchSize)
end

print('===> Unigram loss ' .. unigramLoss(data.train.counts, 
                                          data.test.counts, 
                                          data.train.vocabSize))

if config.epochs > 0 then
   sgvb.trainer.train(model, lossFns, samplers, sgvb.ae, config)
end

if opt.zSize == 2 then
   local labels = data.test.labels
   if labels:dim() > 1 and labels:size(2) > 1 then
      labels = extractTopLabels(labels, numClasses)
   end
   local embedder = createEmbedder(model.encoder, data.train.vocabSize, opt.hiddenSize, opt.zSize)
   sgvb.Visualiser(embedder, opt.numClasses):visualise(data.test.counts, 
                                                       labels)
end
