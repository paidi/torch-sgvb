require 'gnuplot'

local Visualiser = torch.class('nn.Visualiser')

function Visualiser:__init(encoder,k)
   self.model = encoder
   self.k = k
end

function Visualiser:visualise(inputs,labels)
   -- if labels:dim() > 1 then
   --   -- Find most common labels
   --   local _,ranked = torch.sort(labels:sum(1), true)
   --   ranked:resize(ranked:size(2))
   --   local classes = {}
   --   for j=1,self.k do classes[j] = {} end
   -- else  
   -- end

   -- Handle tensor or table-of-tensor input
   local N
   if type(inputs) == 'table' then
      N = #inputs
   else
      N = inputs:size(1)
   end
   
   local classes = {}
   for j=1,self.k do classes[j] = {} end
   for i=1,N do
      local x = {inputs[i]}
      local rep = self.model:forward(x)[1]:resize(2)    
      for j=1,self.k do 
         if labels[i] == j then
            -- print(('Insert (%f,%f) into class %d'):format(rep[1],rep[2],j))
            table.insert(classes[j], {rep[1],rep[2]})
         end
      end
   end
   
   local plotData = {}
   for j=1,self.k do
      if #classes[j] > 0 then
         print(('%d in class %d'):format(#classes[j], j))      
         table.insert(plotData, { torch.Tensor(classes[j]), '+' })
      end
   end
   
   gnuplot.plot(plotData)
end
