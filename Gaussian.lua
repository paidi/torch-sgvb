-- Stochastic layer of Gaussian variables
-- Takes a mean and a stddev vector as input
-- Output are random gaussian samples using the reparameterisation trick
local Gaussian, parent = torch.class('sgvb.Gaussian', 'nn.Module')

function Gaussian:__init()
   parent.__init(self)
   self.gradInput = {}
   self.eps = torch.Tensor()
   self.sigma = torch.Tensor()
end

function Gaussian:updateOutput(input)
   -- input: {mu, log(sigma^2)}
   -- output: mu + eps * sigma
   local mu, logsigma = table.unpack(input)
   if mu:type() ~= self.eps:type() or mu:size() ~= self.eps:size() then
      self.eps = mu:clone()
   end
   if self.sigma ~= logsigma:size() then
      self.sigma = torch.exp(logsigma/2)
   else
      self.sigma:copy(logsigma):div(2):exp()
   end

   if not self.output or self.output:size() ~= mu:size() then
      self.output = mu:clone()
      self.gradInput[1] = torch.zeros(mu:size())
      self.gradInput[2] = torch.zeros(mu:size())
   else
      self.output:copy(mu)
   end

   -- Sample epsilon
   self.eps:randn(self.eps:size())

   -- mean + eps \cdot stddev
   self.output:addcmul(self.eps, self.sigma)

   return self.output
end

function Gaussian:updateGradInput(input, gradOutput)
   local mu, logsigma = table.unpack(input)
   if self.sigma ~= logsigma:size() then
      self.sigma = torch.exp(logsigma/2)
   else
      self.sigma:copy(logsigma):div(2):exp()
   end

   self.gradInput[1]:copy(gradOutput)
   self.gradInput[2]:copy(gradOutput)
   self.gradInput[2]:cmul(self.eps):cmul(self.sigma):mul(0.5)
   return self.gradInput
end
