-- A class to generate samples from a corpus of documents represented
-- as sparse word count tensors, e.g. the document containing 10
-- instances of word 1 and 5 of word 7 would be represented as
-- torch.Tensor{ {1,10}, {5,7} } 
--
-- Note (Paidi): Each batch is returned as a table of sparse tensors,
-- due to the fact that Torch does not support sparse matrices Note
-- 
-- Note (Paidi): Representing documents as sparse tensors means we
-- cannot use the GPU (unless/until we implement cunn.SparseLinear)
local DocumentCountSampler = torch.class('sgvb.DocumentCountSampler')

function DocumentCountSampler:__init(documents, 
                                     vocabSize, 
                                     batchSize, 
                                     oneWordPerSample,
                                     progressInterval)
   
   self.inputs = documents
   self.lengths = torch.Tensor(tablex.map(
                                  function(x) return x[{{},2}]:sum() end, 
                                  self.inputs))
   self.size = #self.inputs
   self.batchSize = batchSize
   
   -- Assume that the batch consists of N samples from a
   -- document-specific unigram distribution?  If this is not true, we
   -- assumed that each document counts as a single sample This is
   -- important to be able to match both forms of PPL computation used
   -- in the literature
   self.oneWordPerSample = oneWordPerSample
   if progressInterval > 0 then
      self.logProgress = true
      self.progressInterval = progressInterval
   else
      self.logProgress = false
   end
end

function DocumentCountSampler:getSamples()
   self.sampleCount = 0
   local batch = 1
   function f()
      if self.logProgress and batch % self.progressInterval == 0 then
         xlua.progress((batch-1)*self.batchSize, self.size)
      end
      if batch*self.batchSize <= self.size then
         local from = (batch-1)*self.batchSize+1
         local to = batch*self.batchSize
         batch = batch + 1      
         if self.oneWordPerSample then
            self.sampleCount = self.sampleCount + self.lengths[{{from,to}}]:sum()
         else
            self.sampleCount = self.sampleCount + self.batchSize
         end
         return {
            input=tablex.sub(self.inputs, from, to), 
            size = self.batchSize 
         }
      end
   end
   return f
end

function DocumentCountSampler:getSampleCount()
   return self.sampleCount
end
