local DocumentCountNLL, parent = torch.class('sgvb.DocumentCountNLL', 'nn.Criterion')

function DocumentCountNLL:__init(documentAverage)
   parent.__init(self)
   self.documentAverage = documentAverage
end

-- Compute NLL for sparse word count tensors
function DocumentCountNLL:updateOutput(logProbs, counts)
   self.output = 0
   for i,x in ipairs(counts) do
      local docNLL = 0.0
      for j=1,x:size(1) do
         docNLL = docNLL - x[{j,2}] * logProbs[{i,x[{j,1}]}]
      end
      if self.documentAverage then
         self.output = self.output + docNLL / x[{{},2}]:sum()
      else
         self.output = self.output + docNLL
      end
   end  
   return self.output
end

-- Compute derivative of NLL for sparse count tensors
function DocumentCountNLL:updateGradInput(logProbs,counts)
   
   if self.gradInput:size() ~= logProbs:size() then
      self.gradInput = torch.zeros(logProbs:size())
   else
      self.gradInput:zero()
   end
   
   for i,x in ipairs(counts) do    
      local Z = 1
      if self.documentAverage then
         Z = x[{{},2}]:sum()
      end
      for j=1,x:size(1) do
         self.gradInput[{i,x[{j,1}]}] = -x[{j,2}]/Z
      end
   end

   return self.gradInput
end
