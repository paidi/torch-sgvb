require 'nn'
require 'AutoEncoder'

local ae = {}

function ae.createTrainingFunction(model, lossFns, config)
  local optimConfig = config.optimParam
  local optimise = optim[optimConfig.name]
  local gradientLogger = config.gradientLogger
  local numLatentSamples = config.numLatentSamples or 1
  local parameters,gradParameters = model:getParameters()

  local reconstructionLossFn, encodingLossFn = table.unpack(lossFns)

  local function train(batch) 
    local input = batch.input

    -- Create a closure to pass to optimisation
    local feval = function(x)
      if x ~= parameters then 
        parameters:copy(x)
      end
      gradParameters:zero()
    
      local loss = 0.0

      -- Compute encoding and encoding loss (if applicable)
      local encoding = model.encoder:forward(input)
      local eLoss = encodingLossFn:forward(encoding)
      local dloss_de = encodingLossFn:backward(encoding)
      
      -- Try to reconstruct the input from the output and propagate
      -- the error back through the decoder
      local rLoss = 0.0
      local drLoss_de = tablex.map(function(x) return torch.zeros(x:size()) end, dloss_de)
      for i=1,numLatentSamples do
        local reconstruction = model.decoder:forward(encoding)
        rLoss = rLoss + reconstructionLossFn:forward(reconstruction, input) / numLatentSamples 
        
        local drLoss_dr = reconstructionLossFn:backward(reconstruction, input)
        drLoss_dr:div(numLatentSamples)

        -- Accumulate gradients on both components of the encoding
        for j,l in ipairs(model.decoder:backward(encoding, drLoss_dr)) do
          drLoss_de[j]:add(l)
        end
      end
      
      for i,l in ipairs(drLoss_de) do
        -- print(('%d: %f %f %f'):format(i, l:min(), l:mean(), l:max()))
        dloss_de[i]:add(l)
      end

      model.encoder:backward(input, dloss_de)

      if gradientLogger then
        gradientLogger:add({
            mean = gradParameters:mean(),
            max = gradParameters:max(),
            min = gradParameters:min()
        })
      end
      return rLoss + eLoss, gradParameters
    end
    
    local _,batchLoss = optimise(feval, parameters, optimConfig)
    
    return batchLoss[1]
  end    
  
  return train
end

function ae.createTestFunction(model, lossFns, config)  
  local reconstructionLossFn, encodingLossFn = table.unpack(lossFns)
  local numLatentSamples = config.numLatentSamples or 1
  local function validate(batch)
    local input = batch.input
    local loss = 0.0

    local encoding = model.encoder:forward(input)
    
    for i=1,numLatentSamples do
      -- Get reconstruction of source data
      local reconstruction = model.decoder:forward(encoding)
      
      -- Compare to input and compute error
      loss =  loss + reconstructionLossFn:forward(reconstruction, input)
    end
    loss = loss / numLatentSamples
    loss = loss + encodingLossFn:forward(encoding)
    return loss
  end
  return validate
end

return ae
