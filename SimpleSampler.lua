local SimpleSampler = torch.class('sgvb.SimpleSampler')

function SimpleSampler:__init(data,labels,batchSize,progressInterval)  
   self.inputs = data
   self.targets = labels
   self.size = self.inputs:size(1)
   self.batchSize = batchSize
   if progressInterval > 0 then
      self.logProgress = true
      self.progressInterval = progressInterval
   else
      self.logProgress = false
   end
end

function SimpleSampler:getSamples()
   self.sampleCount = 0
   local batch = 1
   function f()
      if self.logProgress and batch % self.progressInterval == 0 then
         xlua.progress((batch-1)*self.batchSize, self.size)
      end
      if batch*self.batchSize <= self.size then
         local from = (batch-1)*self.batchSize+1
         local to = batch*self.batchSize
         batch = batch + 1      
         self.sampleCount = self.sampleCount + self.batchSize
         return {
            input=self.inputs[{{from, to},{}}], 
            target=self.targets[{{from, to}}], 
            size = self.batchSize 
         }
      end
   end
   return f
end

function SimpleSampler:getSampleCount()
   return self.sampleCount
end
