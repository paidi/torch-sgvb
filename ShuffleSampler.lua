local ShuffleSampler = torch.class('sgvb.ShuffleSampler')

function ShuffleSampler:__init(data,batchSize,progressInterval)  
   self.inputs = data
   self.size = self.inputs:size(1)
   self.batchSize = batchSize
   if progressInterval > 0 then
      self.logProgress = true
      self.progressInterval = progressInterval
   else
      self.logProgress = false
   end
end

function ShuffleSampler:getSamples()
   self.sampleCount = 0
   local batch = 1
   local N = self.inputs:size(1)
   local shuffle = torch.LongTensor(N):randperm(N)
   function f()
      if self.logProgress and batch % self.progressInterval == 0 then
         xlua.progress((batch-1)*self.batchSize, self.size)
      end
      if batch*self.batchSize <= self.size then
         local from = (batch-1)*self.batchSize+1
         local to = batch*self.batchSize
         batch = batch + 1      
         self.sampleCount = self.sampleCount + self.batchSize
         local data = { input = torch.Tensor(self.batchSize, self.inputs:size(2)),
                        size = self.batchSize }
         for i=from,to do
            local idx = i-from+1
            data.input[{ {idx}, {} }]:copy(self.inputs[{ {shuffle[i]}, {} }])
         end
         
         return data
      end
   end
   return f
end

function ShuffleSampler:getSampleCount()
   return self.sampleCount
end
