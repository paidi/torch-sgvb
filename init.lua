require('torch')
require('nn')
tablex = require('pl.tablex')

sgvb = {}

-- Utilities
torch.include('sgvb', 'ShuffleSampler.lua')
torch.include('sgvb', 'SimpleSampler.lua')

-- SGVB
torch.include('sgvb', 'Gaussian.lua')
torch.include('sgvb', 'GaussianKLCriterion.lua')

-- Topic modelling
torch.include('sgvb', 'DocumentCountNLL.lua')
torch.include('sgvb', 'DocumentCountSampler.lua')
torch.include('sgvb', 'DocumentEncoder.lua')

-- MNIST
torch.include('sgvb', 'Visualiser.lua')

sgvb.trainer = require('trainer.lua')
sgvb.ae = require('ae.lua')

return sgvb
