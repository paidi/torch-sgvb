-- A criterion for computing the KL-divergence between a Gaussian and N(0,I)
-- We assume the Gaussian is presented as a pair of vectors, one for the mean
-- and one for the standard deviation
-- See Kingma and Welling (http://arxiv.org/pdf/1312.6114v10.pdf) Appendix B
local GaussianKLCriterion, parent = torch.class('sgvb.GaussianKLCriterion', 
                                                'nn.Criterion')
function GaussianKLCriterion:__init()
   parent.__init(self)
   self.gradInput = {}
end

function GaussianKLCriterion:updateOutput(input)
   -- input = {mu, log(sigma^2)}
   -- 0.5 * sum( mu^2 + sigma^2 - 1 - log(sigma^2) )
   local mu, logsigmasq = table.unpack(input)
   if not self.divergences or self.divergences:size() ~= mu:size() then
      self.divergences = logsigmasq:clone():mul(-1)
   else
      self.divergences:copy(logsigmasq):mul(-1)
   end
   self.divergences:add(-1)
   self.divergences:add(torch.pow(mu,2))
   self.divergences:add(torch.exp(logsigmasq))
   return self.divergences:sum() * 0.5
end

function GaussianKLCriterion:updateGradInput(input, _)
   -- input = {mu, log(sigma^2)}
   -- gradInput = {mu, 0.5*(sigma^2 - 1)}
   local mu, logsigmasq = table.unpack(input)
   if #self.gradInput == 0 or self.gradInput[1]:size() ~= mu:size() then
      self.gradInput[1] = mu:clone()
      self.gradInput[2] = torch.exp(logsigmasq):add(-1):mul(0.5)
   else
      self.gradInput[1]:copy(mu)
      self.gradInput[2]:copy(logsigmasq):exp():add(-1):mul(0.5)
   end
   return self.gradInput
end
