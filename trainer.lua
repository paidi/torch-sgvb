local trainer = {}

function trainer.test(testFn, sampler)
   local loss = 0.0
   for batch in sampler:getSamples() do
      -- Compare to target and compute error
      loss = loss + testFn(batch)
   end

   return loss/sampler:getSampleCount()
end

function trainer.train(model, lossFns, samplers, spec, config)
   print('===> Training')
   print(config)
   local trainFn = spec.createTrainingFunction(model, lossFns, config)
   local validateFn = spec.createTestFunction(model, lossFns, config)
   local testFn = spec.createTestFunction(model, lossFns, config)
   local trainSampler,validationSampler,testSampler = table.unpack(samplers)

   local improvementRate = config.improvementRate or 1.0
   local stopAfter = config.stopAfter or 10
   local bestValidLoss = 0.0
   if validationSampler then
      bestValidLoss = trainer.test(validateFn, validationSampler)
      print(('===> Before training validation loss: %f'):format(bestValidLoss))
   end

   local stopCounter = 0
   for epoch=1, config.epochs do
      local epochLoss = 0.0
      sys.tic()
      for batch in trainSampler:getSamples() do
         local batchLoss = trainFn(batch)
         epochLoss = epochLoss + batchLoss
      end
      local time = sys.toc()
      local N = trainSampler:getSampleCount()
      print('==> Completed epoch ' .. epoch)

      local logLine = {epoch=epoch,
                       trainLoss=epochLoss/N,
                       msPerSample=time/N}
      if validationSampler then
         local validLoss = trainer.test(validateFn, validationSampler)
         logLine.validLoss = validLoss
         if validLoss < bestValidLoss then
            if config.saveDir then
               torch.save(paths.concat(config.saveDir, 
                                       'parameters.t7'),
                          parameters)
            end
            bestValidLoss = validLoss
         elseif validLoss > improvementRate * bestValidLoss then
            stopCounter = stopCounter + 1
         end
      end

      for k,v in pairs(logLine) do
         print(('===> %s : %f'):format(k,v))
      end
      if config.logger then
         config.logger:add(logLine)
      end

      if stopCounter == stopAfter then break end

   end

   if testSampler then
      local f = assert(io.open(paths.concat(config.saveDir, 
                                            'score.txt'), 
                               'w'))
      f:write(trainer.test(testFn, testSampler))
      f:close()
   end
end

return trainer
