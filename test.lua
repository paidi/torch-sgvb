require 'totem'
require 'nn'
require 'sgvb'

local jac = nn.Jacobian

local tests = {}
local tester = totem.Tester()

local precision = 1E-6

-- Test a criterion
local function criterionJacobianTest1D(cri, input, target)
   local eps = 1e-6   
   local _ = cri:forward(input, target)
   local dfdx = cri:updateGradInput(input, target)

   -- for each input perturbation, do central difference
   local centraldiff_dfdx = torch.Tensor():resizeAs(input):zero()
   local input_s = input:storage()
   local centraldiff_dfdx_s = centraldiff_dfdx:storage()
   for i=1,input:nElement() do
      -- f(xi + h)
      input_s[i] = input_s[i] + eps
      local fx1 = cri:forward(input, target)
      -- f(xi - h)
      input_s[i] = input_s[i] - 2*eps
      local fx2 = cri:forward(input, target)
      -- f'(xi) = (f(xi + h) - f(xi - h)) / 2h
      local cdfx = (fx1 - fx2) / (2*eps)
      -- store f' in appropriate place
      centraldiff_dfdx_s[i] = cdfx
      -- reset input[i]
      input_s[i] = input_s[i] + eps
   end

   -- compare centraldiff_dfdx with :updateGradInput()
   local err = (centraldiff_dfdx - dfdx):abs():max()
   tester:assertlt(err, precision, 'error in difference between central difference ' .. centraldiff_dfdx:sum() .. ' and :updateGradInput ' .. dfdx:sum())
end

-- Wrap a module with table input to take a single tensor as input
function wrap(m, inputSizes)
   local function splitInput(input, inputSizes)
      local inputs = {}
      local from = 1
      for i=1,#inputSizes do         
         local to = from + inputSizes[i] - 1
         inputs[i] = input[{{from,to}}]
         from = to+1
      end
      return inputs
   end

   local function join(inputs, size)
      local result = torch.Tensor(size)
      local from = 1
      for i=1,#inputs do         
         local to = from + inputs[i]:size(1) - 1
         result[{{from,to}}]:copy(inputs[i])
         from = to+1
      end
      return result
   end

   local wrapper = {}
   
   function wrapper:forward(input)
      torch.manualSeed(1)
      self.output = m:forward(splitInput(input, inputSizes))
      return self.output
   end

   function wrapper:updateGradInput(input, gradOutput)
      return join(m:updateGradInput(splitInput(input, inputSizes), gradOutput), input:size())
   end

   function wrapper:accGradParameters(input, gradOutput)      
   end
   
   function wrapper:zeroGradParameters()
   end

   return wrapper
end

function tests.testDocumentCountNLLA()
  local p = torch.Tensor{ {1,2,3,4}, {3,3,3,1} }:div(10):log()
  local counts = { torch.Tensor{ {1,4}, {3,2} }, torch.Tensor{ {4,1}, {1,1} } }

  local cri = sgvb.DocumentCountNLL(false)

  -- Correct output
  tester:asserteq(-4*p[{1,1}] - 2*p[{1,3}] - p[{2,4}] - p[{2,1}], cri:forward(p,counts))
  
  -- Check gradient
  criterionJacobianTest1D(cri, p, counts)
end

function tests.testDocumentCountNLLB()
  local p = torch.Tensor{ {1,2,3,4}, {3,3,3,1} }:div(10):log()
  local counts = { torch.Tensor{ {1,4}, {3,2} }, torch.Tensor{ {4,1}, {1,1} } }

  local cri = sgvb.DocumentCountNLL(true)

  -- Correct output
  tester:asserteq(-(4*p[{1,1}] + 2*p[{1,3}])/6 - (p[{2,4}] + p[{2,1}])/2, cri:forward(p,counts))
  
  -- Check gradient
  criterionJacobianTest1D(cri, p, counts)
end

function tests.testDocumentCountSamplerA()
  local counts = { 
    torch.Tensor{ {1,4}, {3,2} },
    torch.Tensor{ {1,4}, {3,2} }, 
    torch.Tensor{ {1,4}, {3,2} } 
  }
  local sampler = sgvb.DocumentCountSampler(counts,100,1,true,0)
  
  local results = {}
  for r in sampler:getSamples() do table.insert(results, r) end
  
  tester:assertTensorEq(results[1].input[1], counts[1])
  tester:assertTensorEq(results[2].input[1], counts[2])
  tester:assertTensorEq(results[3].input[1], counts[3])
  
  tester:asserteq(sampler:getSampleCount(), 3*6)
end

function tests.testDocumentCountSamplerB()
  local counts = { 
    torch.Tensor{ {1,4}, {3,2} },
    torch.Tensor{ {1,4}, {3,2} }, 
    torch.Tensor{ {1,4}, {3,2} } 
  }
  local sampler = sgvb.DocumentCountSampler(counts,100,1,false,0)
  
  local results = {}
  for r in sampler:getSamples() do table.insert(results, r) end
  
  tester:assertTensorEq(results[1].input[1], counts[1])
  tester:assertTensorEq(results[2].input[1], counts[2])
  tester:assertTensorEq(results[3].input[1], counts[3])
  
  tester:asserteq(sampler:getSampleCount(), 3)
end

function tests.GaussianKLCriterion()
   local N = 20
   local input = torch.rand(2*N)
   local cri = sgvb.GaussianKLCriterion()

   tester:assertgt(wrap(cri, {N,N}):forward(input), 0.0)
   criterionJacobianTest1D(wrap(cri, {N,N}), input)
end

function tests.Gaussian()
   local N = 20
   local input = torch.rand(N*2)
   local target = torch.rand(N)

   local err = jac.testJacobian(wrap(sgvb.Gaussian(), {N,N}), input)
   tester:assertlt(err, 1e-3, 'error on state ')
end

function tests.ShuffleSampler()
  local data = torch.randn(10,5)
  torch.manualSeed(1)
  local shuffle = torch.randperm(10)
  
  local x = {}
  for i=1,2 do
    x[i] = torch.Tensor(5,5)
    for j=1,5 do x[i][j]:copy(data[shuffle[j + 5*(i-1)]]) end
  end
  
  local sampler = sgvb.ShuffleSampler(data,5,0)
  
  local y = {}
  torch.manualSeed(1)
  for batch in sampler:getSamples() do
    table.insert(y, batch.input)
  end
  
  for i=1,2 do
    tester:assertTensorEq(x[i], y[i])
  end
  
  tester:asserteq(10, sampler:getSampleCount())
end
 
return tester:add(tests):run()

